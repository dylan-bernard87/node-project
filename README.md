# Projet Node.js | Hapi BERNARD Dylan

Toutes les routes sont disponibles dans '/documentation'.

Pour envoyer des emails, j'ai utilisé nodemailer et mailtrap qui est un équivalent à ethereal.email.

Cependant, dans le cas où vous utiliseriez mailtrap, il est possible que vous ayez une erreur liée au nombre d'envoi de mails par seconde.
Dans ce cas utilisez ethereal.email ou diminuez le nombre d'utilisateur pour tester.

## Variables d'environnement
Toutes les variables d'environnement se trouvent dans le fichier /server/.env

### Variable base de données
DB_DATABASE correspond au nom de la base de données

DB_HOST correspond à l'adresse de la base de données

DB_USER correspond au nom de l'utilisateur

DB_PASSWORD correspond au mot de passe de l'utilisateur

### Variable d'email
HOST_EMAIL correspond à l'adresse du fake serveur SMTP

PORT_EMAIL correspond au port du fake serveur SMTP

USER_EMAIL correspond au nom d'utilisateur du serveur SMTP

PASS_EMAIL correspond au mot de passe de l'utilisateur du serveur SMTP
