'use strict';

module.exports = {

    async up(knex) {

        await knex.schema.createTable('film_library', (table) => {

            table.increments('id').primary();
            table.string('title').notNull();
            table.text('description').notNull();
            table.string('director').notNull();
            table.dateTime('releaseDate');

            table.dateTime('createdAt').notNull().defaultTo(knex.fn.now());
            table.dateTime('updatedAt').notNull().defaultTo(knex.fn.now());
        });
    },

    async down(knex) {

        await knex.schema.dropTableIfExists('film_library');
    }
};
