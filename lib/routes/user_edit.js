'use strict';

const Joi = require('joi');

module.exports = {
    method: 'PATCH',
    path: '/user/{id}',
    options: {
        tags: ['api'],
        auth: {
            scope: [ 'admin' ]
        },
        validate: {
            params: Joi.object({
                id: Joi.number().required().description('id of the user')
            }),
            payload: Joi.object({
                firstName: Joi.string().required().min(3).example('').description('Firstname of the user'),
                lastName: Joi.string().required().min(3).example('').description('Lastname of the user'),
                email: Joi.string().email().example('').description('email of the user')
            })
        }
    },

    handler: async (request, h) => {
        const id = request.params.id;
        const { userService } = request.services();
        return await userService.edit(request.payload, id);
    }
};
