'use strict';

const Joi = require('joi');

module.exports = {
    method: 'get',
    path: '/user/{id}',
    options: {
        tags: ['api'],
        auth: {
            scope: ['user','admin']
        },
        validate: {
            params: Joi.object({
                id: Joi.number().required().description('id of the user')
            })
        }
    },

    handler: async (request, h) => {
        const id = request.params.id;
        const { userService } = request.services();
        return await userService.getOne(id);
    }
};
