'use strict';

const Joi = require('joi');

module.exports = {
    method: 'DELETE',
    path: '/user/{id}',
    options: {
        tags: ['api'],
        auth: {
            scope: [ 'admin' ]
        },
        validate: {
            params: Joi.object({
                id: Joi.number().required().description('id of the user')
            })
        }
    },

    handler: async (request, h) => {
        const id = request.params.id;
        const { userService } = request.services();
        const resultFromDelete = await userService.delete(id);
        return resultFromDelete == 1 ? '' : 'Not worked';
    }
};
