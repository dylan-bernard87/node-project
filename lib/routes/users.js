'use strict';

const Joi = require('joi');

module.exports = {
    method: 'get',
    path: '/users',
    options: {
        tags: ['api'],
        auth: {
            scope: ['user','admin']
        }
    },

    handler: async (request, h) => {
        const { User } = request.models();

        const user = await User.query();

        return user;
    }
};
