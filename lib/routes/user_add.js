'use strict';

const Joi = require('joi');

module.exports = {
    method: 'post',
    path: '/user/add',
    options: {
        auth: false,
        tags: ['api'],
        validate: {
            payload: Joi.object({
                firstName: Joi.string().required().min(3).example('John').description('Firstname of the user'),
                lastName: Joi.string().required().min(3).example('Doe').description('Lastname of the user'),
                email: Joi.string().email().example('jhon.doe@etu.unilim.fr').description('email of the user'),
                username: Joi.string().example('jhonDoe').description('username of the user'),
                password: Joi.string().min(8).example('passwordDB').description('password of the user')
            })
        }
    },
    handler: async (request, h) => {
        const { userService } = request.services();
        const { emailService } = request.services();
        const result = await userService.create(request.payload);
        if(result.email !== undefined){
            emailService.sendEmailCreation(result.email);
        }

        return result;
    }
};
