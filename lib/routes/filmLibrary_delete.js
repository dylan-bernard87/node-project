'use strict';

const Joi = require('joi');

module.exports = {
    method: 'delete',
    path: '/film-library/delete/{id}',
    options: {
        auth: {
            scope: [ 'admin' ]
        },
        tags: ['api'],
        validate: {
            params: Joi.object({
                id: Joi.number().required().description('id of the film library')
            }),
        }
    },
    handler: async (request, h) => {
        const { filmLibraryService } = request.services();
        const { favoriteService } = request.services();
        const idFilm = request.params.id;

        // Check if favorites are related to the idFilm and remove them before remove the film
        let relationsFilmFavorite = await favoriteService.getFavoriteRelatedToAFilm(idFilm);
        if(relationsFilmFavorite[0] !== undefined) {
            for(let iteRemove = 0; iteRemove < relationsFilmFavorite.length; iteRemove++) {
                favoriteService.remove(idFilm, relationsFilmFavorite[iteRemove].id_user);
            }
        }

        return await filmLibraryService.deleteFilm(idFilm);
    }
};
