'use strict';

const Joi = require('joi');
const Boom = require('@hapi/boom');

module.exports = {
    method: 'delete',
    path: '/favorite/remove/{id}',
    options: {
        auth: {
            scope: [ 'user' ]
        },
        tags: ['api'],
        validate: {
            params: Joi.object({
                id: Joi.number().required().description('id of the film')
            }),
        }
    },
    handler: async (request, h) => {
        const { favoriteService } = request.services();
        const currentIdUser = request.auth.credentials.id;
        const idFilm = request.params.id;

        return await favoriteService.remove(idFilm,currentIdUser);
    }
};
