'use strict';

const Joi = require('joi');
const Boom = require('@hapi/boom');

module.exports = {
    method: 'PATCH',
    path: '/film-library/edit/{id}',
    options: {
        tags: ['api'],
        auth: {
            scope: [ 'admin' ]
        },
        validate: {
            params: Joi.object({
                id: Joi.number().required().description('id of the film library')
            }),
            payload: Joi.object({
                title: Joi.string().min(2).example('').description('Nom du film'),
                description: Joi.string().min(10).example('').description('Description du film'),
                releaseDate: Joi.date(),
                director: Joi.string().min(3).example('').description('Réalisateur du film')
            })
        }
    },

    handler: async (request, h) => {
        const id = request.params.id;
        const { filmLibraryService } = request.services();
        const { emailService } = request.services();
        const { userService } = request.services();

        const film = await filmLibraryService.edit(request.payload, id);

        if(film.id !== undefined){
            // Need to get all users witch had favorite on this film
            const users = await userService.getUsersWithFavorite(film);

            if(users.length > 0){
                for(let i =0; i < users.length;i++) {
                    emailService.sendEmailEditFilm(users[i].email, film.title);
                }
            }
        }

        return film;
    }
};
