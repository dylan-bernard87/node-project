'use strict';

const Joi = require('joi');

module.exports = {
    method: 'post',
    path: '/film-library/add',
    options: {
        auth: {
            scope: [ 'admin' ]
        },
        tags: ['api'],
        validate: {
            payload: Joi.object({
                title: Joi.string().min(2).example('Titanic').description('Nom du film'),
                description: Joi.string().min(10).example('Example de description').description('Description du film'),
                releaseDate: Joi.date(),
                director: Joi.string().min(3).example('Thomas Andrews').description('Réalisateur du film')
            })
        }
    },
    handler: async (request, h) => {
        const { filmLibraryService } = request.services();
        const film = await filmLibraryService.create(request.payload);
        if(film !== undefined){
            const { User } = request.models();
            const users = await User.query();
            const { emailService } = request.services();

            for(let i = 0; i < users.length; i++){
                emailService.sendEmailCreationFilm(users[i].email,film.title);
            }
        }
        return film;
    }
};
