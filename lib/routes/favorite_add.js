'use strict';

const Joi = require('joi');
const Boom = require('@hapi/boom');

module.exports = {
    method: 'post',
    path: '/favorite/add/{id}',
    options: {
        auth: {
            scope: [ 'user' ]
        },
        tags: ['api'],
        validate: {
            params: Joi.object({
                id: Joi.number().required().description('id of the film')
            }),
        }
    },
    handler: async (request, h) => {
        const { filmLibraryService } = request.services();
        const { favoriteService } = request.services();
        const currentIdUser = request.auth.credentials.id;
        const idFilm = request.params.id;

        // Check if the film exist in the database
        const film = await filmLibraryService.getFilm(idFilm);

        if(film instanceof Boom.Boom){
            return film;
        }

        // Check if the favorite exist in the database
        const favoriteExist = await favoriteService.getFavorite(idFilm,currentIdUser);

        if(favoriteExist instanceof Boom.Boom){
            return favoriteExist;
        }

        return await favoriteService.add(idFilm,currentIdUser);
    }
};
