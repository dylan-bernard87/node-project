'use strict';

const Joi = require('joi');

module.exports = {
    method: 'PATCH',
    path: '/user/grant/admin/{id}',
    options: {
        tags: ['api'],
        auth: {
            scope: [ 'admin' ]
        },
        validate: {
            params: Joi.object({
                id: Joi.number().required().description('id of the user')
            })
        }
    },

    handler: async (request, h) => {
        const id = request.params.id;
        const { userService } = request.services();
        return await userService.grant(id);
    }
};
