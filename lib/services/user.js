'use strict';

const { Service } = require('schmervice');
const IutEncrypt = require('db-iut-encrypt');
const Boom = require('@hapi/boom');
const Jwt = require('@hapi/jwt');

module.exports = class UserService extends Service {

    create(user) {
        const { User } = this.server.models();
        user.password = IutEncrypt.encryptSha(user.password);
        return User.query().insertAndFetch(user);
    }

    getOne(id) {
        const { User } = this.server.models();
        return User.query()
            .where('id',id);
    }

    delete(id) {
        const { User } = this.server.models();
        return User.query().deleteById(id);
    }

    async edit(user,id) {
        const { User } = this.server.models();
        let result = await User.query().patchAndFetchById(id,user);

        if(result === undefined)
          return Boom.badData('User inexistant');
        else
          return result;
    }

    async login(user) {
        const { User } = this.server.models();
        const encodedPassword = IutEncrypt.encryptSha(user.password);

        // contain the user witch correspond to the email
        const userTmp = await User.query()
            .where('email',user.email);

        if (userTmp[0] !== undefined && encodedPassword === userTmp[0].password) {
            // return a Jwt token
            return Jwt.token.generate(
                {
                    aud: 'urn:audience:iut',
                    iss: 'urn:issuer:iut',
                    id: userTmp[0].id,
                    firstName: userTmp[0].firstName,
                    lastName: userTmp[0].lastName,
                    email: userTmp[0].email,
                    scope: userTmp[0].scope
                },
                {
                    key: 'iut-db-key-auth', // La clé qui est définit dans lib/auth/strategies/jwt.js
                    algorithm: 'HS512'
                },
                {
                    ttlSec: 14400 // 4 hours
                }
            );
        }

        return Boom.unauthorized('Identifiant et/ou mot de passe invalides');
    }

    async grant(id) {
        const { User } = this.server.models();
        const success = await User.query().findById(id).patch({scope: 'admin'});

        if (success === 1) {
            return 'Operation : "Succesful';
        }

        return Boom.badData('Utilisateur non présent en base de données');
    }

    async getUsersWithFavorite(film) {
        const { User } = this.server.models();
        return await User.query()
            .innerJoin('favorite','user.id','favorite.id_user')
            .where('favorite.id_film',film.id);
    }
};
