'use strict';

const { Service } = require('schmervice');
const Boom = require('@hapi/boom');
const Jwt = require('@hapi/jwt');

module.exports = class FilmLibraryService extends Service {

    create(film) {
        const { FilmLibrary } = this.server.models();
        return FilmLibrary.query().insertAndFetch(film);
    }

    async edit(film,id) {
        const { FilmLibrary } = this.server.models();
        const filmEdited = await FilmLibrary.query().patchAndFetchById(id,film);
        if(filmEdited === undefined){
            return Boom.badData('Film inexistant');
        }

        return filmEdited;
    }

    async getFilm(filmId) {
        const { FilmLibrary } = this.server.models();
        const film =  await FilmLibrary.query().findById(filmId);
        if(film === undefined) {
          return Boom.badData('Film inexistant');
        } else {
            return film;
        }
    }

    async deleteFilm(filmId){
        const { FilmLibrary } = this.server.models();
        const result = await FilmLibrary.query().deleteById(filmId);

        if(result === 1)
          return 'Success';
        else
          return Boom.badData('Ce film n\'existe pas');

    }
};
