'use strict';

const { Service } = require('schmervice');
const Boom = require('@hapi/boom');
const Jwt = require('@hapi/jwt');

module.exports = class FavoriteService extends Service {

    async getFavorite(filmId,userId) {
        const { Favorite } = this.server.models();
        const fav =  await Favorite.query()
            .where('id_user',userId)
            .where('id_film',filmId);

        if(fav[0] === undefined) {
            return true;
        } else {
            return Boom.badData('Favori déja existant');
        }
    }

    add(idFilm,currentIdUser) {
        const { Favorite } = this.server.models();
        const fav = new Favorite();
        fav.id_user = currentIdUser;
        fav.id_film = idFilm;
        return Favorite.query().insertAndFetch(fav);
    }

    async remove(idFilm,currentIdUser) {
        const { Favorite } = this.server.models();
        const result = await Favorite.query()
                        .delete()
                        .where('id_user',currentIdUser)
                        .where('id_film',idFilm);

        if(result === 1) {
            return "Success";
        }
        else {
            return Boom.notImplemented("Vous n'avez pas ce film en favori");
        }
    }

    async getFavoriteRelatedToAFilm(idFilm){
        const { Favorite } = this.server.models();
        const fav =  await Favorite.query()
            .where('id_film',idFilm);

       return fav;
    }
};
