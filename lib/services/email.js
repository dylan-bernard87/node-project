'use strict';

const { Service } = require('schmervice');
const nodemailer = require('nodemailer');

module.exports = class EmailService extends Service {

    transporter = nodemailer.createTransport({
        host: process.env.HOST_EMAIL,
        port: process.env.PORT_EMAIL,
        auth: {
            user: process.env.USER_EMAIL,
            pass: process.env.PASS_EMAIL
        }
    });

    async sendEmailCreation(email) {
        return email = await this.transporter.sendMail({
            from: 'dylan.bernard@etu.unilim.fr',
            to: email,
            subject: 'Création compte node project',
            html: '<h1>Bonjour,</h1> \r\n <p>Un compte vient d\'être créé avec votre adresse email !</p>',
        });
    }

    async sendEmailCreationFilm(email,titreFilm) {
        return email = await this.transporter.sendMail({
            from: 'dylan.bernard@etu.unilim.fr',
            to: email,
            subject: 'Création d\'un film',
            html: `<h1>Bonjour,</h1> \r\n <p>Le film ${titreFilm} vient d'être créer sur le site !</p>`,
        });
    }

    async sendEmailEditFilm(email,titreFilm) {
        return email = await this.transporter.sendMail({
            from: 'dylan.bernard@etu.unilim.fr',
            to: email,
            subject: 'Modification d\'un film',
            html: `<h1>Bonjour,</h1> \r\n <p>Le film ${titreFilm} vient d'être modifié sur le site !</p>`,
        });
    }
};
