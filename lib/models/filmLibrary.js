'use strict';

const Joi = require('joi');
const { Model } = require('schwifty');

module.exports = class FilmLibrary extends Model {

    static get tableName() {

        return 'film_library';
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().greater(0),
            createdAt: Joi.date(),
            updatedAt: Joi.date(),
            title: Joi.string().min(2).example('Titanic').description('Nom du film'),
            description: Joi.string().min(10).example('Example de description').description('Description du film'),
            releaseDate: Joi.date(),
            director: Joi.string().min(3).example('Thomas Andrews').description('Réalisateur du film')
        });
    }

    $beforeInsert(queryContext) {
        this.updatedAt = new Date();
        this.createdAt = this.updatedAt;
    }

    $beforeUpdate(opt, queryContext) {
        this.updatedAt = new Date();
    }
};
